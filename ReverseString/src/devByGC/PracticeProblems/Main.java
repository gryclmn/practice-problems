package devByGC.PracticeProblems;

public class Main {

    public static void main(String[] args) {
	    String originalString = "Hello World";

        System.out.println(reverseString(originalString));
    }

    public static String reverseString(String originalString) {

        String reversedString = "";

        for (int i = originalString.length() - 1; i >= 0; i--) {
            reversedString += originalString.charAt(i);
        }

        return reversedString;
    }
}
