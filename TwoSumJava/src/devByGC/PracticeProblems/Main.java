package devByGC.PracticeProblems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        /*
            Two sum
            Given an array of integers,
            return indices of the two numbers such that they add up to a specific target.
            You may assume that each input would have exactly one solution,
            and you may not use the same element twice.
        */

        int[] numArray = {11, 10, 3, 22, 30};
        int[] result = twoSum(numArray, 13);

        System.out.println(result[0]);
        System.out.println(result[1]);

    }

    public static int[] twoSum(int[] numArray, int target) {

        // Array to store two indexes for the answer
        int[] answer = new int[2];
        // Hashmap to easily track values already examined and access them with O(1) complexity
        // Key = value from numArray, Value = index from numArray
        Map<Integer, Integer> hashMap = new HashMap<Integer, Integer>();

        for (int i = 0; i < numArray.length; i++) {
            // Considering x = target - number,
            // check the hashMap for key = x, thus getting the index of the value
            Integer indexOfValue = hashMap.get(target - numArray[i]);

            if (indexOfValue == null) {
                // If indexOfValue was not found, add the value as key, index as value
                hashMap.put(numArray[i], i);
            } else {
                // If indexOfValue was found then assign the results
                answer[0] = indexOfValue;
                answer[1] = i;
                break;
            }

        }

        return answer;
    }

}