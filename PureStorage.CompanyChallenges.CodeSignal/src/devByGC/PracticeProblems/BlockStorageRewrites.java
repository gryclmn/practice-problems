package devByGC.PracticeProblems;

import java.util.HashMap;
import java.util.Map;

public class BlockStorageRewrites {

    int[][] blockStorageRewrites(int blockCount, int[][] writes, int threshold) {

//        For blockCount = 10, writes = [[0, 4], [3, 5], [2, 6]], and threshold = 2, the output should be
//        blockStorageRewrites(blockCount, writes, threshold) = [[2, 5]].

        Map map = new HashMap();

        for (int i = 0; i < writes.length; i++) {

            int[] currentWrites = writes[i];
            int lastBlock = currentWrites[currentWrites.length - 1];

            for (int currentBlock = currentWrites[0]; currentBlock <= lastBlock; currentBlock++) {

                if (map.containsKey(currentBlock)) {

                } else {
                    map.put(currentBlock, 1);
                }

            }

        }

        int[][] result = {{}};

        return result;

    }

}
