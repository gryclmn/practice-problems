package devByGC.PracticeProblems;

public class Main {

    public static void main(String[] args) {

        int[] sourceArray       = {33531593, 96933415, 28506400, 39457872, 29684716, 86010806};
        int[] destinationArray  = {33531593, 96913415, 28506400, 39457872, 29684716, 86010806};

        int[] result = LongestUncorruptedSegment.longestUncorruptedSegment(sourceArray, destinationArray);
        System.out.println("Result = " + result[0] + ", " + result[1]);
        System.out.println("Answer = 4, 2");


    }




}
