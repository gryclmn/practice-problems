package devByGC.PracticeProblems;

public class LongestUncorruptedSegment {

    public static int[] longestUncorruptedSegment(int[] sourceArray, int[] destinationArray) {
        Boolean isUncorruptedSegment = false;

        int longestLength = 0;
        int longestIndex = 0;

        int currentLength = 0;
        int currentStartingIndex = 0;

        for (int i = 0; i < sourceArray.length; i++) {
            if (sourceArray[i] == destinationArray[i]) {
                System.out.println("Elements are equal: " + sourceArray[i] + " = " + destinationArray[i]);
                if (isUncorruptedSegment) {
                    currentLength++;
                    System.out.printf("isUncorruptedSegment = %s, currentStartingIndex = %d, currentLength = %d \n", isUncorruptedSegment, currentStartingIndex, currentLength);
                } else {
                    isUncorruptedSegment = true;
                    currentStartingIndex = i;
                    currentLength++;
                    System.out.printf("isUncorruptedSegment = %s, currentStartingIndex = %d, currentLength = %d \n", isUncorruptedSegment, currentStartingIndex, currentLength);
                }
            } else if (currentLength > longestLength) {
                longestLength = currentLength;
                longestIndex = currentStartingIndex;
                currentLength = 0;
                currentStartingIndex = 0;
                isUncorruptedSegment = false;
                System.out.printf("longestLength = %d, longestIndex = %d, isUncorruptedSegment = %s, currentStartingIndex = %d, currentLength = %d \n", longestLength, longestIndex, isUncorruptedSegment, currentStartingIndex, currentLength);
            } else {
                currentLength = 0;
                currentStartingIndex = 0;
                isUncorruptedSegment = false;
                System.out.printf("longestLength = %d, longestIndex = %d, isUncorruptedSegment = %s, currentStartingIndex = %d, currentLength = %d \n", longestLength, longestIndex, isUncorruptedSegment, currentStartingIndex, currentLength);
            }
        }

        if (currentLength > longestLength) {
            longestLength = currentLength;
            longestIndex = currentStartingIndex;
            currentLength = 0;
            currentStartingIndex = 0;
            isUncorruptedSegment = false;
            System.out.printf("longestLength = %d, longestIndex = %d, isUncorruptedSegment = %s, currentStartingIndex = %d, currentLength = %d \n", longestLength, longestIndex, isUncorruptedSegment, currentStartingIndex, currentLength);
        }

        int[] result = {longestLength, longestIndex};

        return result;
    }

}
